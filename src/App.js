import React, { useState } from "react";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import "./Skeleton.css";
import "./App.scss";

import ContractItem from "./components/ContractItem";
import AddContrato from "./components/AddContrato";
import { contratos, partes } from "./data/MockData";
import AddParte from "./components/AddParte";
import ParteItem from "./components/ParteItem";

localStorage.setItem("contratos", JSON.stringify(contratos));
localStorage.setItem("partes", JSON.stringify(partes));

function App() {
  const [contratos, setContratos] = useState(JSON.parse(localStorage.getItem("contratos")));
  const [partes, setPartes] = useState(JSON.parse(localStorage.getItem("partes")));

  const onAdd = (obj, isContrato) => {
    console.log(obj, isContrato);
    if (isContrato === true) {
      setContratos([...contratos, obj]);
    } else if (isContrato === false) {
      setPartes([...partes, obj]);
    } else {
      console.log("isContrato nao foi setado corretamente");
    }
  };

  const onDelete = (id, isContrato) => {
    if (isContrato === true) {
      setContratos(contratos.filter(contrato => contrato.id !== id));
    } else if (isContrato === false) {
      setPartes(partes.filter(parte => parte.id !== id));
    } else {
      console.log("isContrato nao foi setado corretamente");
    }
  };

  const onEditSubmit = (obj, isContrato) => {
    if (isContrato === true) {
      setContratos(contratos.map(contrato => (contrato.id === obj.id ? obj : contrato)));
    } else if (isContrato === false) {
      setPartes(partes.map(parte => (parte.id === obj.id ? obj : parte)));
    } else {
      console.log("isContrato nao foi setado corretamente");
    }
  };

  const navbarStyle = {
    textDecoration: "none",
    color: "white",
    margin: "auto 0"
  };

  function Vincular() {
    return <h2>Vincular</h2>;
  }

  return (
    <div className="App">
      <Router>
        <nav className="navbar">
          <h3>Gestão de contratos</h3>
          <div className="links">
            <NavLink style={navbarStyle} to="/">
              <h5>Criar novo</h5>
            </NavLink>
            <NavLink style={navbarStyle} to="/vincular">
              <h5>Vincular</h5>
            </NavLink>
          </div>
        </nav>
        <Route path="/" exact component={Main} />
        <Route path="/vincular" component={Vincular} />
      </Router>
    </div>
  );

  function Main() {
    return (
      <div className="container row">
        <div className="six columns">
          <h2>Contratos Template</h2>

          <AddContrato onAdd={onAdd} />

          <table>
            <thead></thead>
            <tbody>
              {contratos.map(contrato => {
                return (
                  <ContractItem
                    key={contrato.id}
                    {...contrato}
                    onDelete={onDelete}
                    onEditSubmit={onEditSubmit}
                  />
                );
              })}
            </tbody>
          </table>
        </div>
        <div className="six columns">
          <h2>Partes Template</h2>

          <AddParte onAdd={onAdd} />

          <table>
            <thead></thead>
            <tbody>
              {partes.map(parte => {
                return (
                  <ParteItem
                    key={parte.id}
                    {...parte}
                    onDelete={onDelete}
                    onEditSubmit={onEditSubmit}
                  />
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default App;
