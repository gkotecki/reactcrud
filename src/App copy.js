import React from "react";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import "./Skeleton.css";
import "./App.scss";

import ContractItem from "./components/ContractItem";
import AddContrato from "./components/AddContrato";
import { contratos, partes } from "./data/MockData";
import AddParte from "./components/AddParte";
import ParteItem from "./components/ParteItem";

localStorage.setItem("contratos", JSON.stringify(contratos));
localStorage.setItem("partes", JSON.stringify(partes));

function Appcopy()  {
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     contratos: JSON.parse(localStorage.getItem("contratos")),
  //     partes: JSON.parse(localStorage.getItem("partes"))
  //   };

  //   this.onAdd = this.onAdd.bind(this);
  //   this.onDelete = this.onDelete.bind(this);
  //   this.onEditSubmit = this.onEditSubmit.bind(this);
  // }

  const[contratos, getContratos] = useState(JSON.parse(localStorage.getItem("contratos")));
  const[partes, getPartes] = useState(JSON.parse(localStorage.getItem("partes")));

  getContratos() {
    return this.state.contratos;
  }

  getPartes() {
    return this.state.partes;
  }

  onAdd(obj, isContrato) {
    console.log(obj, isContrato);

    if (isContrato === true) {
      const contratos = this.getContratos();
      contratos.push(obj);
      this.setState({ contratos });
    } else if (isContrato === false) {
      const partes = this.getPartes();
      partes.push(obj);
      this.setState({ partes });
    } else {
      console.log("isContrato nao foi setado corretamente");
    }
  }

  onDelete(id, isContrato) {
    if (isContrato === true) {
      const contratos = this.getContratos();
      const filteredContratos = contratos.filter(contrato => {
        return contrato.id !== id;
      });
      this.setState({ contratos: filteredContratos });
    } else if (isContrato === false) {
      const partes = this.getPartes();
      const filteredPartes = partes.filter(parte => {
        return parte.id !== id;
      });
      this.setState({ partes: filteredPartes });
    } else {
      console.log("isContrato nao foi setado corretamente");
    }
  }

  onEditSubmit(obj, isContrato) {
    if (isContrato === true) {
      let contratos = this.getContratos();
      contratos = contratos.map(contrato => {
        if (contrato.id === obj.id) {
          contrato.titulo = obj.titulo;
          contrato.dataIni = obj.dataIni;
          contrato.dataVenc = obj.dataVenc;
          contrato.arquivo = obj.arquivo;
        }
        return contrato;
      });
      this.setState({ contratos });
    } else if (isContrato === false) {
      let partes = this.getPartes();
      partes = partes.map(parte => {
        if (parte.id === obj.id) {
          parte.nome = obj.nome;
          parte.sobrenome = obj.sobrenome;
          parte.email = obj.email;
          parte.cpf = obj.cpf;
          parte.telefone = obj.telefone;
        }
        return parte;
      });
      this.setState({ partes });
    } else {
      console.log("isContrato nao foi setado corretamente");
    }
  }

  Vincular() {
    return <h2>Vincular</h2>;
  }

  render() {
    return (
      <div className="App">
        <Router>
          <nav className="navbar">
            <h3>Gestão de contratos</h3>
            <div className="links">
              {/* <NavLink to="/"> */}
              <h5>Criar novo</h5>
              {/* </NavLink> */}
              {/* <NavLink to="/vincular/"> */}
              <h5>Vincular</h5>
              {/* </NavLink> */}
            </div>
          </nav>
          <div className="container row">
            <div className="six columns">
              <h2>Contratos Template</h2>

              <AddContrato onAdd={this.onAdd} />

              {this.state.contratos.map(contrato => {
                return (
                  <ContractItem
                    key={contrato.id}
                    {...contrato}
                    onDelete={this.onDelete}
                    onEditSubmit={this.onEditSubmit}
                  />
                );
              })}
            </div>
            <div className="six columns">
              <h2>Partes Template</h2>

              <AddParte onAdd={this.onAdd} />

              {this.state.partes.map(parte => {
                return (
                  <ParteItem
                    key={parte.id}
                    {...parte}
                    onDelete={this.onDelete}
                    onEditSubmit={this.onEditSubmit}
                  />
                );
              })}
            </div>
          </div>
          {/* <Route path="/" exact render={this.Main} /> */}
          {/* <Route path="/vincular/" component={this.Vincular} /> */}
        </Router>
      </div>
    );
  }
}

export default Appcopy;
