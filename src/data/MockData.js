const contratos = [
  {
    id: 8111111,
    titulo: "contrato 1",
    dataIni: "2019-08-28",
    dataVenc: "2020-08-28",
    arquivo: "1"
  },
  {
    id: 8111112,
    titulo: "contrato 2",
    dataIni: "2019-01-01",
    dataVenc: "2025-01-01",
    arquivo: "2"
  },
  {
    id: 8111113,
    titulo: "contrato 3",
    dataIni: "2019-08-28",
    dataVenc: "2020-08-28",
    arquivo: "3"
  }
];

// nome, sobrenome, email, CPF e telefone
const partes = [
  {
    id: 9111111,
    nome: "joao",
    sobrenome: "silva",
    email: "joao@joao.com.br",
    cpf: "11122233344",
    telefone: "41123451234"
  },
  {
    id: 9111112,
    nome: "john",
    sobrenome: "souza",
    email: "john@john.com.br",
    cpf: "22233344455",
    telefone: "41123451234"
  },
  {
    id: 9111113,
    nome: "janio",
    sobrenome: "sena",
    email: "janio@janio.com.br",
    cpf: "33344455566",
    telefone: "41123451234"
  }
];

export { contratos };
export { partes };
// export default contratos;
