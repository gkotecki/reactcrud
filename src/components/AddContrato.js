import React from "react";

class AddContrato extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    let contrato = {
      id: Math.floor(Math.random() * (+8999999 - +8000000)) + +8000000,
      titulo: this.tituloInput.value,
      dataIni: this.dataInicioInput.value,
      dataVenc: this.dataVencimentoInput.value,
      arquivo: this.arquivoInput.value
    };
    this.props.onAdd(contrato, true);
    this.tituloInput.value = "";
    this.dataInicioInput.value = "";
    this.dataVencimentoInput.value = "";
    this.arquivoInput.value = "";
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <h3>Novo contrato</h3>
        <label htmlFor="titulo"> Titulo </label>
        <input
          id="titulo"
          type="text"
          placeholder="Titulo"
          className="u-full-width"
          ref={tituloInput => (this.tituloInput = tituloInput)}
        />

        <div className="row">
          <div className="six columns">
            <label htmlFor="dataIni"> Data Inicial </label>
            <input
              id="dataIni"
              type="date"
              className="u-full-width"
              ref={dataInicioInput => (this.dataInicioInput = dataInicioInput)}
            />
          </div>
          <div className="six columns">
            <label htmlFor="dataVenc"> Data de Vencimento </label>
            <input
              id="dataVenc"
              type="date"
              className="u-full-width"
              ref={dataVencimentoInput => (this.dataVencimentoInput = dataVencimentoInput)}
            />
          </div>
        </div>

        <div className="row">
          <label htmlFor="arquivo"> Arquivo </label>
          <input
            id="arquivo"
            type="file"
            accept=".doc,.docx,.pdf"
            className=""
            ref={arquivoInput => (this.arquivoInput = arquivoInput)}
          />
          <button className="button-primary u-pull-right">Adicionar</button>
        </div>

        <hr />
      </form>
    );
  }
}

export default AddContrato;
