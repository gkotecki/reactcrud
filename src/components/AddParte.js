// id: 9111111,
// nome: "joao",
// sobrenome: "silva",
// email: "joao@joao.com.br",
// cpf: "11122233344",
// telefone: "41123451234"
import React from "react";

class AddParte extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    let parte = {
      id: Math.floor(Math.random() * (+9999999 - +9000000)) + +9000000,
      nome: this.nomeInput.value,
      sobrenome: this.sobrenomeInput.value,
      email: this.emailInput.value,
      cpf: this.cpfInput.value,
      telefone: this.telefoneInput.value
    };
    this.props.onAdd(parte, false);
    this.nomeInput.value = "";
    this.emailInput.value = "";
    this.cpfInput.value = "";
    this.telefoneInput.value = "";
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <h3>Nova parte</h3>

        <div className="row">
          <div className="six columns">
            <label htmlFor="nome"> Nome </label>
            <input
              id="nome"
              type="text"
              placeholder="Nome"
              className="u-full-width"
              ref={nomeInput => (this.nomeInput = nomeInput)}
            />
          </div>
          <div className="six columns">
            <label htmlFor="nome"> Sobrenome </label>
            <input
              id="sobrenome"
              type="text"
              placeholder="Sobrenome"
              className="u-full-width"
              ref={sobrenomeInput => (this.sobrenomeInput = sobrenomeInput)}
            />
          </div>
        </div>

        <div className="row">
          <div className="six columns">
            <label htmlFor="email"> Email </label>
            <input
              id="email"
              type="email"
              placeholder="asdas@asdas.com.br"
              className="u-full-width"
              ref={emailInput => (this.emailInput = emailInput)}
            />
          </div>
          <div className="six columns">
            <label htmlFor="cpf"> CPF </label>
            <input
              id="cpf"
              type="text"
              placeholder="12345678900"
              className="u-full-width"
              ref={cpfInput => (this.cpfInput = cpfInput)}
            />
          </div>
        </div>

        <div className="row">
          <label htmlFor="telefone"> Telefone </label>
          <input
            id="telefone"
            type="tel"
            placeholder="41 12345 1234"
            className=""
            ref={telefoneInput => (this.telefoneInput = telefoneInput)}
          />
          <button className="button-primary u-pull-right">Adicionar</button>
        </div>

        <hr />
      </form>
    );
  }
}

export default AddParte;
