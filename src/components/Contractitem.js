import React from "react";

class ContractItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isEdit: false
    };

    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEditSubmit = this.onEditSubmit.bind(this);
  }

  onDelete() {
    this.props.onDelete(this.props.id, true);
  }

  onEdit() {
    this.setState({ isEdit: true });
  }

  onEditSubmit(event) {
    event.preventDefault();
    let contrato = {
      id: this.props.id,
      titulo: this.tituloInput.value,
      dataIni: this.dataInicioInput.value,
      dataVenc: this.dataVencimentoInput.value,
      arquivo: this.arquivoInput.value
    };
    this.props.onEditSubmit(contrato, true);
    this.setState({ isEdit: false });
  }

  render() {
    const { id, titulo, dataIni, dataVenc, arquivo } = this.props;

    return (
      <div>
        {this.state.isEdit ? (
          <form onSubmit={this.onEditSubmit}>
            <input
              placeholder="Titulo"
              type="text"
              ref={tituloInput => (this.tituloInput = tituloInput)}
              defaultValue={titulo}
            />
            <input
              placeholder="Data Inicial"
              type="date"
              ref={dataInicioInput => (this.dataInicioInput = dataInicioInput)}
              defaultValue={dataIni}
            />
            <input
              placeholder="Data de Vencimento"
              type="date"
              ref={dataVencimentoInput => (this.dataVencimentoInput = dataVencimentoInput)}
              defaultValue={dataVenc}
            />
            <input
              placeholder="Arquivo"
              type="file"
              accept=".pdf,.doc,.docx"
              ref={arquivoInput => (this.arquivoInput = arquivoInput)}
            />
            <button>Save</button>
          </form>
        ) : (
          <tr>
            <td>{titulo}</td>
            <td>{dataIni}</td>
            <td>{dataVenc}</td>
            <td>{arquivo}</td>
            <td>
              <button onClick={this.onEdit}>Edit</button>
            </td>
            <td>
              <button onClick={this.onDelete}>Delete</button>
            </td>
          </tr>
        )}
      </div>
    );
  }
}

export default ContractItem;
