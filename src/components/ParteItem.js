import React from "react";

class ParteItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isEdit: false
    };

    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEditSubmit = this.onEditSubmit.bind(this);
  }

  onDelete() {
    this.props.onDelete(this.props.id, false);
  }

  onEdit() {
    this.setState({ isEdit: true });
  }

  onEditSubmit(event) {
    event.preventDefault();
    let parte = {
      id: this.props.id,
      nome: this.nomeInput.value,
      sobrenome: this.sobrenomeInput.value,
      email: this.emailInput.value,
      cpf: this.cpfInput.value,
      telefone: this.telefoneInput.value
    };
    this.props.onEditSubmit(parte, false);
    console.log(parte);

    this.setState({ isEdit: false });
  }

  render() {
    const { id, nome, sobrenome, email, cpf, telefone } = this.props;

    return (
      <div>
        {this.state.isEdit ? (
          <form onSubmit={this.onEditSubmit}>
            <input
              placeholder="Nome"
              type="text"
              ref={nomeInput => (this.nomeInput = nomeInput)}
              defaultValue={nome}
            />
            <input
              placeholder="Sobrenome"
              type="text"
              ref={sobrenomeInput => (this.sobrenomeInput = sobrenomeInput)}
              defaultValue={sobrenome}
            />
            <input
              placeholder="Email"
              type="email"
              ref={emailInput => (this.emailInput = emailInput)}
              defaultValue={email}
            />
            <input
              placeholder="cpf"
              type="text"
              ref={cpfInput => (this.cpfInput = cpfInput)}
              defaultValue={cpf}
            />
            <input
              placeholder="Telefone"
              type="text"
              ref={telefoneInput => (this.telefoneInput = telefoneInput)}
            />
            <button>Save</button>
          </form>
        ) : (
          <tr>
            <td>{nome}</td>
            <td>{sobrenome}</td>
            <td>{email}</td>
            <td>{cpf}</td>
            <td>{telefone}</td>
            <td>
              <button onClick={this.onEdit}>Edit</button>
            </td>
            <td>
              <button onClick={this.onDelete}>Delete</button>
            </td>
          </tr>
        )}
      </div>
    );
  }
}

export default ParteItem;
